package com.example.jobmooij.mooijtjes;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddProductActivity extends AppCompatActivity {
    private SQLiteDatabase mydatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        mydatabase = openOrCreateDatabase("productsdatabase.db", MODE_PRIVATE, null);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(InputActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        int id = R.id.inputEAN;
        if (message.length() >= 3 && message.length() <= 6){
            id = R.id.inputNASA;
        }
        EditText inputEANEditText = findViewById(id);
        inputEANEditText.setText(message);
    }
    public void onCancelClick(View v) {
        finish();
    }
    public void onConfirmClick(View v) {
        boolean valid = true;
        TextView inputEAN = findViewById(R.id.inputEAN);
        TextView inputNASA = findViewById(R.id.inputNASA);
        TextView inputName = findViewById(R.id.inputName);
        TextView inputContent = findViewById(R.id.inputContent);
        TextView inputPrice = findViewById(R.id.inputPrize);
        TextView inputStock = findViewById(R.id.inputStock);
        if (inputEAN.getText().toString().length() != 13) {
            inputEAN.setError("Enter EAN correctly (13 digits)");
            valid = false;
        } else {
            inputEAN.setError(null);
        }
        if (inputNASA.getText().length() < 3 || inputNASA.length() > 6) {
//            inputNASA.setText("100");
            inputNASA.setError("Enter NASA correctly (3-6 digits)");
            valid = false;
        } else {
            inputNASA.setError(null);
        }
        if (inputName.getText().length() < 5) {
//            inputName.setText("Delicata reep test");
            inputName.setError("Enter name correctly");
            valid = false;
        } else {
            inputName.setError(null);
        }
        if (inputContent.getText().length() < 3) {
//            inputContent.setText("69 gram");
            inputContent.setError("Enter content correctly (write fully)");
            valid = false;
        } else {
            inputContent.setError(null);
        }
        try {
            Float.parseFloat(inputPrice.getText().toString());
            inputPrice.setError(null);
        } catch (NumberFormatException e) {
//            inputPrice.setText("0.69");
            inputPrice.setError("Enter Price correctly (with two decimals)");
            valid = false;
        }
        if (inputStock.getText().length() == 0) {
//            inputStock.setText("69");
            inputStock.setError("Enter stock correctly");
            valid = false;
        } else {
            inputStock.setError(null);
        }
        if (valid){
            try {
                mydatabase.execSQL("INSERT INTO Products VALUES (" + inputEAN.getText().toString() + ", " + inputNASA.getText().toString() + ", '" + inputName.getText().toString() + "', '" + inputContent.getText().toString() + "', " + inputPrice.getText().toString() + ", " + inputStock.getText().toString() + ");");
                Toast toast = Toast.makeText(getApplicationContext(), "Succesfully added this product!", Toast.LENGTH_SHORT);
                toast.show();
                finish();
            } catch (SQLiteConstraintException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "Looks like you're adding a known product, check the EAN- or NASA codes again!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
