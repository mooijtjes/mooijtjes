package com.example.jobmooij.mooijtjes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
    public void onLookupClick(View v) {
        Intent intent = new Intent(HomeActivity.this, InputActivity.class);
        startActivity(intent);
    }
    public void onUsedClick(View v) {
        Intent intent = new Intent(HomeActivity.this, ScanActivity.class);
        startActivity(intent);
//        Toast toast = Toast.makeText(this, "Ah-oh it doesn't exist yet", Toast.LENGTH_LONG);
//        toast.show();
    }
}
