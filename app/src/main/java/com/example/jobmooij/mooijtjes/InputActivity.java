package com.example.jobmooij.mooijtjes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.jobmooij.mooijtjes.EANOrNASAINPUT";
    private SQLiteDatabase mydatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        mydatabase = openOrCreateDatabase("productsdatabase.db", MODE_PRIVATE, null);
//        mydatabase.execSQL("DROP TABLE IF EXISTS Products;");
//        mydatabase.execSQL("CREATE TABLE 'Products' ( 'EAN' BIGINT NOT NULL CHECK(LENGTH ( EAN ) = 13) UNIQUE, 'NASA' MEDIUMINT NOT NULL CHECK(LENGTH ( NASA ) >= 3 AND LENGTH ( NASA ) <= 6) UNIQUE, 'Name' VARCHAR ( 255 ) NOT NULL CHECK(LENGTH(Name)>5), 'Size' VARCHAR ( 25 ) NOT NULL CHECK(LENGTH ( Size ) > 3), 'CurrentPrice' DOUBLE CHECK('Currentprice' > 0.00), 'Stock' SMALLINT CHECK('Stock' >= 0) DEFAULT 0, PRIMARY KEY('EAN','NASA') );");
//        mydatabase.execSQL("INSERT INTO Products VALUES (8796549843255, 106, 'Delicata Reep Melk', '200 gram', 1.39, 0), (8796549843256, 107, 'Delicata Reep puur 58%', '200 gram', 1.49, 0)");
    }

    public void onSearchClick(View v) {
        EditText editText = findViewById(R.id.inputEANOrNASA);
        String message = editText.getText().toString();
        Cursor resultSet = mydatabase.rawQuery("SELECT * FROM Products WHERE EAN ='" + message + "' OR NASA ='" + message + "';", null);
        if (resultSet.getCount() > 0) {
            Intent intent = new Intent(InputActivity.this, ShowInfoActivity.class);
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        } else {
            Intent intent = new Intent(InputActivity.this, AddProductActivity.class);
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
        resultSet.close();
    }

    public void onClearClick(View v){
        EditText editText = findViewById(R.id.inputEANOrNASA);
        editText.setText("");
    }
}
