package com.example.jobmooij.mooijtjes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ShowInfoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);
        SQLiteDatabase mydatabase = openOrCreateDatabase("productsdatabase.db", MODE_PRIVATE, null);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(InputActivity.EXTRA_MESSAGE);
        Cursor resultSet = mydatabase.rawQuery("SELECT Name, EAN, NASA FROM Products WHERE EAN ='" + message + "' OR NASA ='" + message + "';", null);
        resultSet.moveToFirst();
        TextView productName = findViewById(R.id.productName);
        productName.setText(resultSet.getString(0));
        TextView EAN = findViewById(R.id.EANNumberTextView);
        EAN.setText(resultSet.getString(1));
        TextView NASA = findViewById(R.id.NASANumberTextview);
        NASA.setText(resultSet.getString(2));
        System.out.println(resultSet.getString(2));
        resultSet.close();
    }

    public void onHomeClick(View v) {
        Intent intent = new Intent(ShowInfoActivity.this, HomeActivity.class);
        startActivity(intent);
    }
    public void onBackClick(View v) {
        finish();
    }
}
